package stjepan.v19z1;

import android.app.Activity;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Random;
import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends Activity {

    @BindView(R.id.rlLayout) RelativeLayout rlLayout;
    @BindView(R.id.tvQuote) TextView tvQuote;
    @BindView(R.id.ivPicture) ImageView ivPicture;

    private final Random rnd = new Random();
    int[] colorsBackgroundArray;
    int[] colorsTextArray;
    TypedArray imagesArray;
    String[] quotesArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeArrays();
        ButterKnife.bind(this);
        initializeUI();
        changeUI();
    }

    private void initializeUI() {
        rlLayout.setBackgroundColor(colorsBackgroundArray[getRandomNum(colorsBackgroundArray.length)]);
        tvQuote.setText(quotesArray[getRandomNum(quotesArray.length)]);
        tvQuote.setTextColor(colorsTextArray[getRandomNum(colorsTextArray.length)]);
        ivPicture.setImageResource(imagesArray.getResourceId(getRandomNum(imagesArray.length()), -1));
    }

    private void initializeArrays() {
        quotesArray = getResources().getStringArray(R.array.QuotesArray);
        colorsBackgroundArray = getResources().getIntArray(R.array.colorsBackground);
        colorsTextArray = getResources().getIntArray(R.array.colorsText);
        imagesArray = getResources().obtainTypedArray(R.array.ImagesArray);
    }

    private void changeUI() {
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                initializeUI();
                changeUI();
            }
        };
        handler.postDelayed(r, getTimeInterval());
    }

    // int randomNum = ThreadLocalRandom.current().nextInt(5000, 12000 + 1); ne moze zbog verzije
    private int getTimeInterval(){
        int timeInterval = 5000 + rnd.nextInt(12000);
        return timeInterval;
    }
    private int getRandomNum(int upper){
        int randomNum = 0 + rnd.nextInt(upper);
        return randomNum;
    }


}
